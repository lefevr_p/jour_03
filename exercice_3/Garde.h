#ifndef GARDE_H_
# define GARDE_H_

typedef enum {STARK, LANNISTER,BARATHEON, TARGARYEN, MARTELL, TYRELL}Maison;
  
  typedef struct s_Corbeau
  {
    char *_nom;
    int  _age;
    Maison _maison;
  }Corbeau;
	      
   void garde_de_nuit(Corbeau *pers,char *nom, int age, Maison maison);

#endif /* !GARDE_H_*/
